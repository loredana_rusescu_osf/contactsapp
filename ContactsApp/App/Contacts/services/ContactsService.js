﻿(function () {

    angular.module('contactsApp')
           .service('ContactsService', ['$http', '$q', ContactsService]);

    function ContactsService($http, $q) {

        var self = this;
        var contactsUrl = 'http://localhost:51661/api/Contacts';

        self.getContacts = function () {
            var deferred = $q.defer();
            $http.get(contactsUrl)
                 .then(function (result) {
                     deferred.resolve(result.data);
                 },
                 function (result, status) {
                     deferred.reject({ result: result, status: status, message: "Contacts were not able to be retrieved" });
                 });

            return deferred.promise;
        };

        self.getContactById = function (id) {
            var deferred = $q.defer();
            $http.get(contactsUrl + '/' + id)
                  .then(function (result) {
                      deferred.resolve(result.data);
                  },
                 function (result, status) {
                     deferred.reject({ result: result, status: status, message: "Contact was not able to be retrieved" });
                 });
            return deferred.promise;
        };

        self.addContact = function (data) {
            var deferred = $q.defer();
            $http.post(contactsUrl, data)
                 .then(function (result) {
                    deferred.resolve(result.data);
                 },
                 function (result, status) {
                     deferred.reject({ result: result, status: status, message: "Contact were not able to be added" });
                 });
            return deferred.promise;
        };

        self.updateContact = function (data) {
            var deferred = $q.defer();
            $http.put(contactsUrl + '/' + data.Id, data)
                        .then(function (result) {
                            deferred.resolve(result.data);
                        },
                         function (result, status) {
                             deferred.reject({ result: result, status: status, message: "Contact were not able to be updated" });
                         });
            return deferred.promise;
        };

        self.deleteContact = function (id) {
            var deferred = $q.defer();
            $http.delete(contactsUrl + '/' + id)
                       .then(function (result) {
                           deferred.resolve(result.data);
                       },
                        function (result, status) {
                            deferred.reject({ result: result, status: status, message: "Contact were not able to be deleted" });
                        });
            return deferred.promise;
        };
    }
}());