﻿app.directive('contactsGrid', function (ContactsService) {
    return {
        restrict: 'E',
        replace: true,
        scope: {
            gridData: '=',
            refresh: '&'
        },
        templateUrl: 'App/Contacts/directives/contactsGrid.html', 
        link: function (scope, element, attrs) {
            var contactsGridColumnDefinitions =
                                                  [
                                                  { name: "Prenume", field: "FirstName" },
                                                  { name: "Nume", field: "LastName" },
                                                  { name: "Adresa", field: "Address" },
                                                  { name: "Telefon", field: "TelephoneNumber" },
                                                  {
                                                      name: "Edit", width: 30,
                                                      displayName: '',
                                                      cellTemplate: "<div><a href='#/{{row.entity.Id}}'><span class='glyphicon glyphicon-pencil'></span></a></div>",
                                                      enableFiltering: false
                                                  },
                                                  {
                                                      name: "Delete", width: 30,
                                                      displayName: '',
                                                      cellTemplate: "<div class='ui-grid-cell-contents'><a href='javascript:void(0)' ng-click='grid.appScope.remove(row.entity.Id)' ><span class='glyphicon glyphicon-trash'></span></a></div>",
                                                      enableFiltering: false
                                                  }
                                                  ];


            scope.gridOptions = {
                data: 'gridData',
                enableFiltering: true,
                columnDefs: contactsGridColumnDefinitions,
                onRegisterApi: function (gridApi) {
                    scope.contactsGrid = gridApi;
                }
            };

            scope.remove = function(id){
                if (!confirm('Are you sure you want to remove this contact?')) return;
                    ContactsService.deleteContact(id).then(function (response) {
                        scope.refresh();
                    });
               
            };
        }
    };
});

