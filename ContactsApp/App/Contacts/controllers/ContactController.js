﻿
app.controller('ContactController', ['$scope', 'ContactsService', '$routeParams', '$location', function ($scope, ContactsService, $routeParams, $location) {
    function getContactInfo(data) {
        var contactImage = data.Image;
        if ((contactImage !== undefined) && (contactImage !== null)) {
            contactImage = contactImage.base64;
        }
        return { Id: data.Id || 0, FirstName: data.FirstName, LastName: data.LastName, Address: data.Address, TelephoneNumber: data.TelephoneNumber, Image: contactImage };
    }

    function redirectToHomepage() {
        $location.url("/");
    }

    function populateContactInfo(id) {
        ContactsService.getContactById(id).then(function (result) {
            $scope.Id = result.Id;
            $scope.FirstName = result.FirstName;
            $scope.LastName = result.LastName;
            $scope.Address = result.Address;
            $scope.TelephoneNumber = result.TelephoneNumber;
            $scope.Image = result.Image;
        });
    }

    function populateEditingModel() {
        if ($routeParams.id > 0) {
            $scope.isEditing = true;
            populateContactInfo($routeParams.id);
        }
    }

    $scope.saveContactInfo = function (data) {
        var contact = getContactInfo(data);
        if ($scope.isEditing) {
            ContactsService.updateContact(contact).then(function (response) {
                redirectToHomepage();
            });;
        }
        else {
            ContactsService.addContact(contact).then(function (response) {
                redirectToHomepage();
            });;
        }
    }

    $scope.getContactImage = function (img) {
        if (img === undefined || img === null) {
            return img;
        }
        var imageToBeReturned = 'data:image/jpeg;base64,';
        if (img.base64 === undefined) {
            imageToBeReturned += img;
        } else {
            imageToBeReturned += img.base64;
        }
        return imageToBeReturned;
    };

    

    populateEditingModel();
}]);
