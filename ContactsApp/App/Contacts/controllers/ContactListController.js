﻿
app.controller('ContactListController', ['$scope', 'ContactsService', '$routeParams', function ($scope, ContactsService, $routeParams) {
    $scope.populateContactsGrid = function() {
        ContactsService.getContacts().then(function (response) {
            $scope.contacts = response;
        });
    }

    $scope.populateContactsGrid();
}]);
