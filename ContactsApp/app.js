﻿var app = angular.module('contactsApp', ['ui.grid', 'ngRoute', 'naif.base64']);

app.config(['$routeProvider', function ($routeProvider) {
    $routeProvider
     .when('/', {
         controller: "ContactListController",
         templateUrl: "App/Contacts/views/contactList.html"
     })
     .when('/:id', {
         controller: 'ContactController',
         templateUrl: 'App/Contacts/views/contact.html'
     })
    .otherwise({ redirectTo: '/' });
}]);

